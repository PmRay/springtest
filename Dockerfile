FROM maven:3-jdk-8-alpine

#setting working directory in the container
WORKDIR /app

#copy file, compile source code and package it as jar
COPY pom.xml /app/
RUN mvn clean package -DskipTests

# ENV PORT 5000
# EXPOSE $PORT
# CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]

#Using official OpenJDK image 
FROM openjdk:17

# Set the working directory in the container
WORKDIR /app

# Copy the built JAR file from the previous stage to the container
COPY target/demo-0.0.1-SNAPSHOT.jar /app/application.jar

# Set the command to run the application
ENTRYPOINT ["java", "-jar", "application.jar"]